package com.example.spinner_adapter_demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list = arrayOf("Mohali","Phase1","Phase2","Phase3","Phase4")

        val spinnerAdapter : Custom_Adapter = Custom_Adapter(applicationContext,list)
        spinner?.adapter = spinnerAdapter
        spinner.onItemSelectedListener=this

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, view: View?, p2: Int, p3: Long) {
    }
}