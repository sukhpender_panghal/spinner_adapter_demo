package com.example.spinner_adapter_demo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.custom_spinner_items.view.*

class Custom_Adapter(var context : Context, var list : Array<String>) : BaseAdapter() {

    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        var view= LayoutInflater.from(context).inflate(R.layout.custom_spinner_items,null)
        view.textView.setText(list[p0])
        return view
    }

    override fun getItem(p0: Int): Any? {
        return p0
    }
    override fun getItemId(p0: Int): Long {
        return 0
    }
    override fun getCount(): Int {
        return list.size
    }
}